import string

badInputError = False

q = 1
p = 1
e = 1
d = 1


def is_prime(num: int) -> bool:
    notprime = False
    for div in range(2, num):
        if num % div == 0:
            notprime = True

    return not notprime


def fi(num1: int, num2: int):
    return (num1 - 1) * (num2 - 1)


def lnko(num1: int, num2: int):
    while num2 > 0:
        prev = num2
        num2 = num1 % num2
        num1 = prev

    return num1

abc = string.ascii_lowercase

def changeAlphabet(alphabet: str):
    global abc
    abc = alphabet


def input_n(n_temp: int):
    global badInputError
    global n
    if n_temp < len(abc):
        badInputError = f"Az N kulcs nem lehet kisebb a választott ábécé hosszánál ({len(abc)})"
    else:
        n = n_temp


def getBadInputError():
    return badInputError


def resetBadInputError():
    global badInputError
    badInputError = False


def refresh_n():
    global n
    n_temp = p * q
    if n_temp < len(abc):
        badInputError = f"Az N kulcs nem lehet kisebb a választott ábécé hosszánál ({len(abc)})"
    else:
        n = n_temp


def refresh_d():
    global d
    d = calculate_d(e, fi(p, q))


def input_p(p_temp: int):
    global badInputError
    global p
    if is_prime(p_temp):
        p = p_temp
        refresh_n()
    else:
        badInputError = "P és Q legyenek prímszámok"


def input_q(q_temp: int):
    global badInputError
    global q
    if is_prime(q_temp):
        q = q_temp
        refresh_n()
    else:
        badInputError = "P és Q legyenek prímszámok"


def calculate_d(e: int, mod: int):
    k = 1
    while True:
        d = (1 + (k * mod)) / e
        if d % 1 == 0 and d != p and d != q:
            return int(d)
        else:
            k += 1


def input_e(e_temp: int, is_for_encoding: bool = False):
    global badInputError
    global e
    if is_for_encoding:
        e = e_temp
    else:
        if lnko(e, fi(p, q)) == 1:
            e = e_temp
        else:
            badInputError = "E és (P - 1)*(Q - 1) legyenek relatív prímek"


def betu_kodol(betu: chr):
    kod_betu = abc.find(betu)

    kod_betu = int(kod_betu)

    kod_betu = (kod_betu ** e) % n
    return kod_betu


def betu_dekodol(szam: int):
    d = calculate_d(e, fi(p, q))
    mv = (szam ** d) % n
    mv = abc[mv]
    return mv


def kodol(m: str):
    kodolt = ""
    for i, b in enumerate(m):
        kodolt += str(betu_kodol(b)) + (" " if i < len(m) else "")

    return kodolt


def dekodol(kod: str):
    mv = ""
    for sz in kod.strip().split(" "):
        mv += betu_dekodol(int(sz))

    return mv
