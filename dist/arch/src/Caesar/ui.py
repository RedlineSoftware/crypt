import caesar
from tkinter import *

root = Tk()
root.title("Caesar")

message = StringVar()
offset = StringVar()

result = StringVar()


def encrypt_button_press():
    result.set(caesar.main_coding_decoding(message.get(), int(offset.get()), False))

def decrypt_button_press():
    result.set(caesar.main_coding_decoding(message.get(), int(offset.get()), True))

message_entry_label = Label(root, text="Message:")
message_entry_label.pack()
message_entry = Entry(root, textvariable=message)
message_entry.pack()

offset_entry_label = Label(root, text="Offset:")
offset_entry_label.pack()
offset_entry = Entry(root, textvariable=offset)
offset_entry.pack()

encrypt_button = Button(root, text="Encrypt", command=encrypt_button_press)
decrypt_button = Button(root, text="Decrypt", command=decrypt_button_press)
encrypt_button.pack()
decrypt_button.pack()

result_label = Label(root, text="Result:")
result_label.pack()

result_textbox = Entry(root, state="readonly", textvariable=result)
result_textbox.pack()


root.mainloop()