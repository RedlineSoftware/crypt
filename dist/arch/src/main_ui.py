from tkinter import *
from os import system
import pathlib

# Színek az /src/style.py-ból másolva.
# Colors copied from /src/style.py
BG_COLOR = "#2E3440"
FG_COLOR = "#ECEFF4"

"""
# Régi kód
# Legacy code
WIN_WIDTH = 500
WIN_HEIGHT = 500
"""
BTTN_WIDTH = 30
BTTN_HEIGHT = 10


program_path = pathlib.Path(__file__).parent.absolute()


win = Tk()
win.title("CRVER - Welcome")

# win.geometry(f"{500}x500")
win.iconphoto(False, PhotoImage(file=str(program_path) + "/icon.png"))
win.config(bg=BG_COLOR)

title_label = Label(win, text="CRVER", font=("sans-serif 30 bold italic"), bg=BG_COLOR, fg=FG_COLOR)
title_label.grid(row=1, column=1, columnspan=6)


class cipher:
    name: str
    desc: str
    grid_pos: list[int]
    grid_colspan: int
    button_bg_color: str

    def __init__(self, nm: str, dsc: str, func, gp: list[int], cs: int, btn_background_color):
        self.name = nm
        self.desc = dsc
        self.function = func
        self.grid_pos = gp
        self.grid_colspan = cs
        self.button_bg_color = btn_background_color


def launch_RSA():
    win.destroy()
    system("python3 RSA/ui.py")


def launch_Caesar():
    system("python3 Caesar/ui_.py")


def launch_ROT13():
    system("python3 ROT13/ui.py")


def launch_Enigma():
    system("python3 Enigma/enigma.py")


def launch_Vigenere():
    system("python3 Vigenere/GUI.py")


ciphers = [
    cipher("ROT13", "Shift 13", launch_ROT13, [2, 1], 2, "#88C0D0"),
    cipher("Caesar", "Simple shift cipher", launch_Caesar, [2, 3], 2, "#88C0D0"),
    cipher("Vigenère", "More complex shift cipher", launch_Vigenere, [2, 5], 2, "#88C0D0"),
    cipher("RSA", "Secure public key encryption", launch_RSA, [3, 1], 3, "#5E81AC"),
    cipher("Enigma", "Electro-mechanical device simulator", launch_Enigma, [3, 4], 3, "#5E81AC")
]

buttons = []

for c in ciphers:
    buttons.append(Button(win, text=c.name + "\n" + c.desc, width=BTTN_WIDTH, height=BTTN_HEIGHT, bg=c.button_bg_color,
                          borderwidth=0, border=None, command=c.function))

row_num = 1
col_num = 1

for i in range(len(ciphers)):
    buttons[i].grid(row=ciphers[i].grid_pos[0], column=ciphers[i].grid_pos[1], columnspan=ciphers[i].grid_colspan)

win.mainloop()
