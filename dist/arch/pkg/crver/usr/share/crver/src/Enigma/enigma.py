from tkinter import *
from tkinter.font import names
from typing import Sized
import pathlib
rotor_abc=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
rotor_one=['U','L','R','B','Q','O','I','F','T','N','G','D','Z','V','W','S','A','H','Y','P','E','K','C','M','J','X',"Rotor I"]
rotor_two=['S','E','D','L','K','U','J','I','V','T','P','R','N','W','F','X','O','M','G','Z','A','H','Q','Y','B','C',"Rotor II"]
rotor_thr=['X','U','L','P','J','H','K','E','F','W','Y','T','B','I','M','O','C','Z','A','R','D','S','G','V','N','Q',"Rotor III"]
rotor_fou=['L','E','Y','J','V','C','N','I','X','W','P','B','Q','M','D','R','T','A','K','Z','G','F','U','H','O','S',"Rotor IV"]
rotor_fiv=['Q','W','E','R','T','Z','U','I','O','A','S','D','F','G','H','J','K','P','Y','X','C','V','B','N','M','L',"Rotor V"]
def mainly(char,rotation_one,rotation_two,rotation_three,plug_setup,pos):
    def first_rotor(char,reflected):            
        outputchar=0
        #rotation over max check
        if ord(char)-65+rotation_one>25:
            outputchar=ord(char)-91+rotation_one
        else:
            outputchar=ord(char)-65+rotation_one
        #direction check
        if reflected:
            reflection=pos[0].index(char)
            # rotation under min check
            if reflection-rotation_one<0:
                return chr(65+(26-(rotation_one-reflection)))

            else:
                return chr(65+reflection-rotation_one)
        else:
            #output fetch
            outputchar=pos[0][outputchar]
            return outputchar

    def second_rotor(char,reflected):
        outputchar=0
        #rotation over max check
        if ord(char)-65+rotation_two>25:
            outputchar=ord(char)-91+rotation_two
        else:
            outputchar=ord(char)-65+rotation_two
        #direction check
        if reflected:
            reflection=pos[1].index(char)
            # rotation under min check
            if reflection-rotation_two<0:
                return chr(65+(26-(rotation_two-reflection)))

            else:
                return chr(65+reflection-rotation_two)
        else:
            #output fetch
            outputchar=pos[1][outputchar]
            return outputchar

    def third_rotor(char,reflected):
        outputchar=0
        #rotation over max check
        if ord(char)-65+rotation_three>25:
            outputchar=ord(char)-91+rotation_three
        else:
            outputchar=ord(char)-65+rotation_three
        #direction check
        if reflected:
            reflection=pos[2].index(char)
            # rotation under min check
            if reflection-rotation_three<0:
                return chr(65+(26-(rotation_three-reflection)))

            else:
                return chr(65+reflection-rotation_three)
        else:
            #output fetch
            outputchar=pos[2][outputchar]
            return outputchar

    def reflector(char):
        #                     A   B   C   D   E   F   G   H   I   J   K   L   M   N   O   P   Q   R   S   T   U   V   W   X   Y   Z
        reflect_connections=['E','O','I','F','A','D','N','P','C','S','Q','Z','T','G','B','H','K','W','J','M','Y','X','R','V','U','L']
        return reflect_connections[ord(char)-65]

            #encryption
    for plug in plug_setup:
        if char in plug:
            char=plug[1-plug.index(char)]
            ##print(char)
    char=first_rotor(char,False)
    char=second_rotor(char,False)
    char=third_rotor(char,False)
    char=reflector(char)
    char=third_rotor(char,True)
    char=second_rotor(char,True)
    char=first_rotor(char,True)
    for plug in plug_setup:
        if char in plug:
            char=plug[1-plug.index(char)]
    #rotation
    up_first(1)
    return char

#Main
def brain(event):
    if event.char.upper() in rotor_abc:
        t_encoded.config(state="normal")
        t_encoded.insert(END,mainly(event.char.upper(),rto,rtw,rtt,pluggerss,positions))
        t_encoded.config(state="disabled")
    elif event.char==' ':
        t_encoded.config(state="normal")
        t_encoded.insert(END,' ')
        t_encoded.config(state="disabled")


rto=0
rtw=0
rtt=0
first=rotor_one
second=rotor_two
third=rotor_thr
current_color=-1
tlist=[]
pluggerss=[]
i=[0,1,2]
color_num=[0,0]
rotors=[rotor_one,rotor_two,rotor_thr,rotor_fou,rotor_fiv]
is_pressed=[False for x in range(26)]
def choose_rotor(place):
    global rotors,first,second,third,positions
    if i[place]==4:
        i[place]=0
    else:
        i[place]+=1  
    positions[place]=(rotors[i[place]])
    positions[place+3].set(positions[place][26])

def up_first(way):
    global rto
    if way>0:
        if rto==25:
            rto=0
            up_second(1)
        else:
            rto+=way
    else:
        if rto==0:
            rto=25
        else:
            rto+=way
    text_first.set(rto)
def up_second(wayy):
    global rtw
    if wayy>0:
        if rtw==25:
            rtw=0
            up_third(1)
        else:
            rtw+=wayy
    else:
        if rtw==0:
            rtw=25
        else:
            rtw+=wayy
    text_second.set(rtw)

def up_third(wayyy):
    global rtt
    if wayyy>0:
        if rtt==25:
            rtt=0
        else:
            rtt+=wayyy
    else:
        if rtt==0:
            rtt=25
        else:
            rtt+=wayyy
    text_third.set(rtt)

def cleartext():
    t_encoded.config(state="normal")
    t_encoded.delete(1.0,END)
    t_encoded.config(state="disabled")

def clearall():
    global rto,rtw,rtt,tlist,pluggerss,text_first,text_second,text_third,color_num
    rto=0
    rtw=0
    rtt=0
    text_first.set(0)
    text_second.set(0)
    text_third.set(0)
    tlist=[]
    pluggerss=[]
    positions[0]=rotor_one
    positions[1]=rotor_two
    positions[2]=rotor_thr
    text_rotor_st.set(positions[0][26])
    text_rotor_nd.set(positions[1][26])
    text_rotor_rd.set(positions[2][26])
    color_num=[0,0]
    for state in range(len(is_pressed)):
        is_pressed[state]=False
    for child in f_abc.winfo_children():
        child.config(bg="#6C5B4C")
    


def pluggers(button):
    global tlist,color_num,current_color
    color=["red","blue","green","purple","pink","yellow","lightblue","lightgreen","cyan","magenta","lightyellow","lime","darkred"]
    if len(tlist)==1 and is_pressed[ord(button)-65]==True and button not in tlist:
        return 0
    else:
        if is_pressed[ord(button)-65]==False:
            if color_num[0]==2:
                color_num[0]=0
                color_num[1]+=1
            if len(tlist)==1:
                tlist.append(button)
                pluggerss.append(tlist)
                tlist=[]
            else:
                tlist.append(button)
            f_abc.nametowidget(button.lower()).config(bg=color[color_num[1]])
            color_num[0]+=1
            is_pressed[ord(button)-65]=True
            if current_color!=-1:
                color_num[1]=current_color
                current_color=-1
        else:
            if len(tlist)==1:
                tlist.pop(0)
            else:
                for y in range(len(pluggerss)):
                    if button in pluggerss[y]:
                        if f_abc.nametowidget(button.lower()).cget("bg")!=color[color_num[1]]:
                            for where_color in range(len(color)):
                                if color[where_color]==f_abc.nametowidget(button.lower()).cget("bg"):
                                    current_color=color_num[1]
                                    color_num[1]=where_color
                        pluggerss[y].remove(button)
                        tlist.append(pluggerss[y][0])
                        pluggerss.pop(y)
                        break
                f_abc.nametowidget(button.lower()).config(bg="#6C5B4C")
            color_num[0]-=1
            is_pressed[ord(button)-65]=False
    
win=Tk()
win.title("Enigma")
win.config(bg="#2E3440")
win.geometry("500x500")
program_path = pathlib.Path(__file__).parent.parent.absolute()
win.iconphoto(False, PhotoImage(file=str(program_path) + "/icon.png"))




#Texts
text_first=StringVar()
text_second=StringVar()
text_third=StringVar()
text_rotor_st=StringVar()
text_rotor_nd=StringVar()
text_rotor_rd=StringVar()
text_first.set(rto)
text_second.set(rtw)
text_third.set(rtt)
text_rotor_st.set(first[26])
text_rotor_nd.set(second[26])
text_rotor_rd.set(third[26])
#Sorry for intruding mon master was lazy
positions=[first,second,third,text_rotor_st,text_rotor_nd,text_rotor_rd]
#Bye
#Frames
f_top=Frame(win,bg="#2E3440")
f_top.grid(row=0,column=0,columnspan=4)

f_abc=Frame(win,bg="#2E3440")
f_abc.grid(row=9,column=0,columnspan=5,padx=40)

f_buttons=Frame(win,bg="#2E3440")
f_buttons.grid(row=1,rowspan=3,column=0,columnspan=4)

#Buttons
    #Rotation
b_first=Button(f_top,textvariable=text_rotor_st,command=lambda: choose_rotor(0),width=10)
b_first.grid(row=0,column=0)

b_second=Button(f_top,textvariable=text_rotor_nd,command=lambda: choose_rotor(1),width=10)
b_second.grid(row=0,column=1)

b_third=Button(f_top,textvariable=text_rotor_rd,command=lambda: choose_rotor(2),width=10)
b_third.grid(row=0,column=2)

b_first_up=Button(f_buttons,text="+",command=lambda: up_first(1),width=10)
b_first_up.grid(row=0,column=0)

b_first_down=Button(f_buttons,text="-",command=lambda: up_first(-1),width=10)
b_first_down.grid(row=2,column=0)

b_second_up=Button(f_buttons,text="+",command=lambda: up_second(1),width=10)
b_second_up.grid(row=0,column=1)

b_second_down=Button(f_buttons,text="-",command=lambda: up_second(-1),width=10)
b_second_down.grid(row=2,column=1)

b_third_up=Button(f_buttons,text="+",command=lambda: up_third(1),width=10)
b_third_up.grid(row=0,column=2)

b_third_down=Button(f_buttons,text="-",command=lambda: up_third(-1),width=10)
b_third_down.grid(row=2,column=2)

    #Clear
b_clear=Button(f_top,text="clear text",width=10,command=cleartext)
b_clear.grid(row=0,column=3,padx=10)

b_clearall=Button(f_buttons,text="clear all",width=10,command=clearall)
b_clearall.grid(row=0,column=3,padx=10)
#STOP DON'T VENTURE FURTHER
b_A=Button(f_abc,name='a',text='A',width=2,height=2,command=lambda: pluggers(b_A.winfo_name().upper()))
b_A.grid(row=0,column=0,padx=2)

b_B=Button(f_abc,name='b',text='B',width=2,height=2,command=lambda: pluggers(b_B.winfo_name().upper()))
b_B.grid(row=0,column=1,padx=2)

b_C=Button(f_abc,name='c',text='C',width=2,height=2,command=lambda: pluggers(b_C.winfo_name().upper()))
b_C.grid(row=0,column=2,padx=2)

b_D=Button(f_abc,name='d',text='D',width=2,height=2,command=lambda: pluggers(b_D.winfo_name().upper()))
b_D.grid(row=0,column=3,padx=2)

b_E=Button(f_abc,name='e',text='E',width=2,height=2,command=lambda: pluggers(b_E.winfo_name().upper()))
b_E.grid(row=0,column=4,padx=2)

b_F=Button(f_abc,name='f',text='F',width=2,height=2,command=lambda: pluggers(b_F.winfo_name().upper()))
b_F.grid(row=0,column=5,padx=2)

b_G=Button(f_abc,name='g',text='G',width=2,height=2,command=lambda: pluggers(b_G.winfo_name().upper()))
b_G.grid(row=0,column=6,padx=2)

b_H=Button(f_abc,name='h',text='H',width=2,height=2,command=lambda: pluggers(b_H.winfo_name().upper()))
b_H.grid(row=0,column=7,padx=2)

b_I=Button(f_abc,name='i',text='I',width=2,height=2,command=lambda: pluggers(b_I.winfo_name().upper()))
b_I.grid(row=0,column=8,padx=2)

b_J=Button(f_abc,name='j',text='J',width=2,height=2,command=lambda: pluggers(b_J.winfo_name().upper()))
b_J.grid(row=0,column=9,padx=2)

b_K=Button(f_abc,name='k',text='K',width=2,height=2,command=lambda: pluggers(b_K.winfo_name().upper()))
b_K.grid(row=0,column=10,padx=2)

b_L=Button(f_abc,name='l',text='L',width=2,height=2,command=lambda: pluggers(b_L.winfo_name().upper()))
b_L.grid(row=0,column=11,padx=2)

b_M=Button(f_abc,name='m',text='M',width=2,height=2,command=lambda: pluggers(b_M.winfo_name().upper()))
b_M.grid(row=0,column=12,padx=2)

b_N=Button(f_abc,name='n',text='N',width=2,height=2,command=lambda: pluggers(b_N.winfo_name().upper()))
b_N.grid(row=1,column=0,padx=2)

b_O=Button(f_abc,name='o',text='O',width=2,height=2,command=lambda: pluggers(b_O.winfo_name().upper()))
b_O.grid(row=1,column=1,padx=2)

b_P=Button(f_abc,name='p',text='P',width=2,height=2,command=lambda: pluggers(b_P.winfo_name().upper()))
b_P.grid(row=1,column=2,padx=2)

b_Q=Button(f_abc,name='q',text='Q',width=2,height=2,command=lambda: pluggers(b_Q.winfo_name().upper()))
b_Q.grid(row=1,column=3,padx=2)

b_R=Button(f_abc,name='r',text='R',width=2,height=2,command=lambda: pluggers(b_R.winfo_name().upper()))
b_R.grid(row=1,column=4,padx=2)

b_S=Button(f_abc,name='s',text='S',width=2,height=2,command=lambda: pluggers(b_S.winfo_name().upper()))
b_S.grid(row=1,column=5,padx=2)

b_T=Button(f_abc,name='t',text='T',width=2,height=2,command=lambda: pluggers(b_T.winfo_name().upper()))
b_T.grid(row=1,column=6,padx=2)

b_U=Button(f_abc,name='u',text='U',width=2,height=2,command=lambda: pluggers(b_U.winfo_name().upper()))
b_U.grid(row=1,column=7,padx=2)

b_V=Button(f_abc,name='v',text='V',width=2,height=2,command=lambda: pluggers(b_V.winfo_name().upper()))
b_V.grid(row=1,column=8,padx=2)

b_W=Button(f_abc,name='w',text='W',width=2,height=2,command=lambda: pluggers(b_W.winfo_name().upper()))
b_W.grid(row=1,column=9,padx=2)

b_X=Button(f_abc,name='x',text='X',width=2,height=2,command=lambda: pluggers(b_X.winfo_name().upper()))
b_X.grid(row=1,column=10,padx=2)

b_Y=Button(f_abc,name='y',text='Y',width=2,height=2,command=lambda: pluggers(b_Y.winfo_name().upper()))
b_Y.grid(row=1,column=11,padx=2)

b_Z=Button(f_abc,name='z',text='Z',width=2,height=2,command=lambda: pluggers(b_Z.winfo_name().upper()))
b_Z.grid(row=1,column=12,padx=2)

#Labels
l_first=Label(f_buttons,bg="#D1D2D8",textvariable=text_first,width=10,height=5)
l_first.grid(row=1,column=0)

l_second=Label(f_buttons,bg="#D1D2D8",textvariable=text_second,width=10,height=5)
l_second.grid(row=1,column=1)

l_third=Label(f_buttons,bg="#D1D2D8",textvariable=text_third,width=10,height=5)
l_third.grid(row=1,column=2)
#TextBoxes
t_encoded=Text(win,width=50,height=10,bg="#908778")
t_encoded.config(state="disabled")
t_encoded.grid(row=4,rowspan=5,column=0,columnspan=10,pady=10,padx=20)
#color
for child in f_top.winfo_children():
    child.config(bg="#6C5B4C")
for child in f_buttons.winfo_children():
    child.config(bg="#6C5B4C")
for child in f_abc.winfo_children():
    child.config(bg="#6C5B4C")
win.bind("<KeyPress>",brain)
win.mainloop()
 