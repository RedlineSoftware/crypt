from string import ascii_lowercase as s

def GenerateTable():
    global table
    table = [s[i:]+s[:i] for i in range(len(s))]
    table = [convert(j) for j in table]

def Encrypt(string,key):
    emessage = ""

    for p in range(len(string)):
        if string[p] not in s:
            emessage += "?"
        else:
            emessage += table[s.index(string[p])][s.index(key[p])]
        
    return emessage

def Decrypt(estring,key): 
    dmessage = ""

    for k in range(len(estring)):
        if estring[k] not in s:
            dmessage += "?"
        else:
            dmessage += table[0][table[s.index(key[k])].index(estring[k])]

    return dmessage

def GenerateKey(string,key):
    if len(string) != len(key):
        for i in range(len(string) - len(key)):
            key += key[i % len(key)]
            
    return(key)

def convert(str_):
    lst1 = []
    lst1[:0] = str_
    return lst1