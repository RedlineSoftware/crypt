import string
from tkinter import *
import rsa
from os import system
import pathlib

# Színek az /src/style.py-ból másolva.
# Colors copied from /src/style.py
BG_COLOR = "#2E3440"
FG_COLOR = "#ECEFF4"
ENTRY_BG_COLOR = "#3B4252"
BUTTON_BG_COLOR = "#4C566A"

win = Tk()
win.title("RSA")
win.resizable(width=False, height=False)
win.geometry("600x400+0+0")
win.config(bg=BG_COLOR)

program_path = pathlib.Path(__file__).parent.parent.absolute()
win.iconphoto(False, PhotoImage(file=str(program_path) + "/icon.png"))

encrypt_message = StringVar()
encrypt_n = StringVar()
encrypt_e = StringVar()

decrypt_code = StringVar()
decrypt_n = StringVar()
decrypt_p = StringVar()
decrypt_q = StringVar()
decrypt_e = StringVar()

result = StringVar()

charsetToApply = StringVar()

charsets = {
    "English lowercase letters": string.ascii_lowercase,
    "English alphabet": string.ascii_letters,
    "English alphabet + digits": string.ascii_letters + string.digits,
    "English alphabet + digits + punctuation": string.ascii_letters + string.digits + string.punctuation,
    "Magyar ábécé": "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz "
}

charset_keys = charsets.keys()


def applyAlphabet(haveto):  # Nem működik pos arg nélkül, nem tudom, miért.
    rsa.changeAlphabet(charsets[charsetToApply.get()])


def showBadInputLabel(override_message=None):
    badInputLabel.config(text=override_message if override_message else rsa.badInputError)
    badInputLabel.pack()


def hideBadInputLabel():
    badInputLabel.pack_forget()


def encrypt_button_press():
    rsa.resetBadInputError()
    rsa.input_n(int(encrypt_n.get()))
    rsa.input_e(int(encrypt_e.get()), is_for_encoding=True)
    print(rsa.getBadInputError())
    if rsa.getBadInputError():
        showBadInputLabel()
    else:
        hideBadInputLabel()
        encodingResult = rsa.kodol(encrypt_message.get())
        result.set(encodingResult)


def decrypt_button_press():
    rsa.resetBadInputError()
    rsa.input_p(int(decrypt_p.get()))
    rsa.input_q(int(decrypt_q.get()))
    rsa.input_e(int(decrypt_e.get()), is_for_encoding=False)
    rsa.refresh_d()

    if rsa.getBadInputError():
        showBadInputLabel()
    else:
        hideBadInputLabel()
        if decrypt_code.get().replace(" ", "").isdigit():
            decodingResult = rsa.dekodol(decrypt_code.get())
            result.set(decodingResult)
        else:
            showBadInputLabel(override_message="Bad input: Code must be numbers separated by spaces")


def back_to_main():
    win.destroy()
    system("python3 main_ui.py")


def clear():
    encrypt_message_entry.delete(0, END)
    encrypt_n_entry.delete(0, END)
    encrypt_e_entry.delete(0, END)
    decrypt_code_entry.delete(0, END)
    decrypt_p_entry.delete(0, END)
    decrypt_q_entry.delete(0, END)
    decrypt_e_entry.delete(0, END)
    result_textbox.delete("1.0", "end")


def copyans():
    win.clipboard_clear()
    win.clipboard_append(result.get().strip())


encrypt_x = 20
horizontal_offset = 300
decrypt_x = encrypt_x + horizontal_offset
main_label_y = 40
offset = 20

back_button = Button(win, text="Back", bg=BUTTON_BG_COLOR, fg=FG_COLOR, command=back_to_main)
back_button.grid(row=0, column=0)

# clear
clearb = Button(win, text="CLEAR", bg="#1AFF1A", command=clear)
clearb.grid(row=0, column=2, columnspan=2, sticky=W)

# copy
copyb = Button(win, bg="yellow", command=copyans, text="COPY")
copyb.grid(row=0, column=1, sticky=E)


encrypt_label = Label(win, text="Encrypt RSA", fg=FG_COLOR, bg=BG_COLOR)
encrypt_message_entry_label = Label(win, text="Message:", fg=FG_COLOR, bg=BG_COLOR)
encrypt_message_entry = Entry(win, textvariable=encrypt_message)

encrypt_label.place(x=encrypt_x, y=main_label_y)
encrypt_message_entry_label.place(x=encrypt_x, y=main_label_y + offset)
win.update()
encrypt_message_entry.place(x=encrypt_x + encrypt_message_entry_label.winfo_width() + 5, y=main_label_y + offset)

encrypt_n_entry_label = Label(win, text="N key:", fg=FG_COLOR, bg=BG_COLOR)
encrypt_n_entry = Entry(win, textvariable=encrypt_n)

win.update()

encrypt_n_entry_label.place(x=encrypt_x,
                            y=encrypt_message_entry.winfo_y() + encrypt_message_entry.winfo_height() + offset)
win.update()
encrypt_n_entry.place(x=encrypt_x + encrypt_message_entry_label.winfo_width() + 5,
                      y=encrypt_message_entry.winfo_y() + encrypt_message_entry.winfo_height() + offset)

win.update()

encrypt_e_entry_label = Label(win, text="E key:", fg=FG_COLOR, bg=BG_COLOR)
encrypt_e_entry = Entry(win, textvariable=encrypt_e)

encrypt_e_entry_label.place(x=encrypt_x, y=encrypt_n_entry.winfo_y() + encrypt_n_entry.winfo_height() + offset)
win.update()
encrypt_e_entry.place(x=encrypt_x + encrypt_message_entry_label.winfo_width() + 5,
                      y=encrypt_n_entry.winfo_y() + encrypt_n_entry.winfo_height() + offset)

win.update()

encrypt_bttn = Button(win, text="Encrypt", command=encrypt_button_press)
encrypt_bttn.place(x=encrypt_x, y=encrypt_e_entry_label.winfo_y() + encrypt_e_entry_label.winfo_height() + offset)

win.update()

result_textbox = Entry(win, state="readonly", width=20, textvariable=result)
result_textbox.place(x=encrypt_x, y=encrypt_bttn.winfo_y() + encrypt_bttn.winfo_height() + offset)

win.update()

decrypt_label = Label(win, text="Decrypt RSA", fg=FG_COLOR, bg=BG_COLOR)
decrypt_code_entry_label = Label(win, text="Code:", fg=FG_COLOR, bg=BG_COLOR)
decrypt_code_entry = Entry(win, textvariable=decrypt_code)

decrypt_label.place(x=decrypt_x, y=main_label_y)
decrypt_code_entry_label.place(x=decrypt_x, y=main_label_y + offset)
win.update()
decrypt_code_entry.place(x=decrypt_x + decrypt_code_entry_label.winfo_width() + 5, y=main_label_y + offset)

decrypt_p_entry_label = Label(win, text="P key:", fg=FG_COLOR, bg=BG_COLOR)
decrypt_p_entry = Entry(win, textvariable=decrypt_p)
win.update()
decrypt_p_entry_label.place(x=decrypt_x, y=decrypt_code_entry.winfo_y() + decrypt_code_entry.winfo_height() + offset)
win.update()

decrypt_p_entry.place(x=decrypt_x + decrypt_code_entry_label.winfo_width() + 5,
                      y=decrypt_code_entry_label.winfo_y() + decrypt_code_entry.winfo_height() + offset)

decrypt_q_entry_label = Label(win, text="Q key:", fg=FG_COLOR, bg=BG_COLOR)
decrypt_q_entry = Entry(win, textvariable=decrypt_q)
win.update()
decrypt_q_entry_label.place(x=decrypt_x, y=decrypt_p_entry.winfo_y() + decrypt_p_entry.winfo_height() + offset)
win.update()
decrypt_q_entry.place(x=decrypt_x + decrypt_code_entry_label.winfo_width() + 5,
                      y=decrypt_p_entry.winfo_y() + decrypt_p_entry.winfo_height() + offset)

decrypt_e_entry_label = Label(win, text="E key:", fg=FG_COLOR, bg=BG_COLOR)
decrypt_e_entry = Entry(win, textvariable=decrypt_e)
win.update()
decrypt_e_entry_label.place(x=decrypt_x, y=decrypt_q_entry.winfo_y() + decrypt_q_entry.winfo_height() + offset)
win.update()
decrypt_e_entry.place(x=decrypt_x + decrypt_e_entry_label.winfo_width() + 5,
                      y=decrypt_q_entry.winfo_y() + decrypt_q_entry.winfo_height() + offset)

decrypt_bttn = Button(win, text="Decrypt", command=decrypt_button_press)
decrypt_bttn.place(x=decrypt_x, y=decrypt_e_entry_label.winfo_y() + decrypt_e_entry_label.winfo_height() + offset)

badInputLabel = Label(win, text="Bad input: p, q must be primes, e, n must be coprimes", fg="red")

win.update()

alphabetLabel = Label(win, text="Alphabet: ", fg=FG_COLOR, bg=BG_COLOR)
alphabetLabel.place(x=encrypt_x, y=result_textbox.winfo_y() + offset + 10)

win.update()

alphabetSwitcher = OptionMenu(win, charsetToApply, *charset_keys, command=applyAlphabet)
alphabetSwitcher.place(x=alphabetLabel.winfo_x() + alphabetLabel.winfo_width() + 10,
                       y=result_textbox.winfo_y() + offset + 10)

win.update()

win.mainloop()
