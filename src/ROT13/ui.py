from turtle import width
import rot13
from tkinter import *
import pathlib
from os import system

root = Tk()
root.title("ROT13")
root.resizable(False, False)
program_path = pathlib.Path(__file__).parent.parent.absolute()
root.iconphoto(False, PhotoImage(file=str(program_path) + "/icon.png"))

# Copied from style.py
# A style.py-ból bemásolva
BG_COLOR = "#2E3440"
FG_COLOR = "#ECEFF4"
ENTRY_BG_COLOR = "#3B4252"
BUTTON_BG_COLOR = "#4C566A"

root.config(bg=BG_COLOR)


def clear():
    message_entry.delete(0, END)
    result_box.delete("1.0", "end")


def copyans():
    root.clipboard_clear()
    root.clipboard_append(result_box.get("1.0", END).strip())


def encode():
    result_box.delete("1.0", END)
    result_box.insert("1.0", rot13.main_encode_decode(message_entry.get(), False))


def decode():
    result_box.delete("1.0", END)
    result_box.insert("1.0", rot13.main_encode_decode(message_entry.get(), True))


def back_to_main():
    root.destroy()


back_button = Button(root, text="Back", bg=BUTTON_BG_COLOR, fg=FG_COLOR, command=back_to_main)
back_button.place(x=0, y=0)

# clear
clearb = Button(root, text="CLEAR", bg="#1AFF1A", command=clear)
clearb.grid(row=0, column=1, columnspan=2, sticky=W)

# copy
copyb = Button(root, bg="yellow", command=copyans, text="COPY")
copyb.grid(row=0, column=0, sticky=E)

message_entry_label = Label(root, text="Message:", bg=BG_COLOR, fg=FG_COLOR)
message_entry = Entry(root)
encode_bttn = Button(root, text="Encode", bg=BUTTON_BG_COLOR, fg=FG_COLOR, command=encode)
decode_bttn = Button(root, text="Decode", bg=BUTTON_BG_COLOR, fg=FG_COLOR, command=decode)

message_entry_label.grid(row=0, column=0)
message_entry.grid(row=1, column=0)
encode_bttn.grid(row=2, column=0, padx=4, pady=2, sticky=W)
decode_bttn.grid(row=2, column=0, padx=4, pady=2, sticky=E)

result_box = Text(root, width=50, height=5, relief=SUNKEN)
result_box.grid(row=6, column=0)

root.mainloop()
