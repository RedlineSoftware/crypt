from multiprocessing.sharedctypes import Value
import caesar as c
from tkinter import *
import pathlib

root = Tk()
root.title("Caesar")
root.resizable(False, False)

program_path = pathlib.Path(__file__).parent.parent.absolute()
root.iconphoto(False, PhotoImage(file=str(program_path) + "/icon.png"))


# Copied from style.py
# A style.py-ból bemásolva
BG_COLOR = "#2E3440"
FG_COLOR = "#ECEFF4"
ENTRY_BG_COLOR = "#3B4252"
BUTTON_BG_COLOR = "#4C566A"

root.config(bg=BG_COLOR)

# VALUEERROR?
def encrypt_button_press():
    outp.delete("1.0", "end")
    try:
        outp.insert("1.0", c.main_coding_decoding(input.get(), int(shiftinp.get()), False))
    except ValueError:
        outp.insert("1.0", "Invalid offset!")


# VALUEERROR?
def decrypt_button_press():
    outp.delete("1.0", "end")
    try:
        outp.insert("1.0", c.main_coding_decoding(input.get(), int(shiftinp.get()), True))
    except ValueError:
        outp.insert("1.0", "Invalid offset!")


def clear():
    input.delete(0, END)
    shiftinp.delete(0, END)
    outp.delete("1.0", "end")


def copyans():
    root.clipboard_clear()
    root.clipboard_append(outp.get("1.0", END).strip())


def back_to_main():
    root.destroy()

back_button = Button(root, text="Back", bg=BUTTON_BG_COLOR, fg=FG_COLOR, command=back_to_main)
back_button.grid(row=0, column=0)


# clear
clearb = Button(root, text="CLEAR", bg="#1AFF1A", command=clear)
clearb.grid(row=0, column=1, columnspan=2, sticky=W)

# copy
copyb = Button(root, bg="yellow", command=copyans, text="COPY")
copyb.grid(row=0, column=0, sticky=E)

# input
text1 = Label(root, text="Input", bg=BG_COLOR, fg=FG_COLOR)
text2 = Label(root, text="Offset", bg=BG_COLOR, fg=FG_COLOR)
input = Entry(root, width=45, relief=SUNKEN)
shiftinp = Entry(root, relief=SUNKEN)
ebutton = Button(root, text="Encrypt!", bg=BUTTON_BG_COLOR, fg=FG_COLOR, command=encrypt_button_press)
dbutton = Button(root, text="Decrypt!", bg=BUTTON_BG_COLOR, fg=FG_COLOR, command=decrypt_button_press)

text1.grid(row=1, column=0)
input.grid(row=2, column=0)
text2.grid(row=3, column=0)
shiftinp.grid(row=4, column=0)
ebutton.grid(row=5, column=0, columnspan=2, sticky=W)
dbutton.grid(row=5, column=0, columnspan=2, sticky=E)

# output
outp = Text(root, width=50, height=5, relief=SUNKEN)
outp.grid(row=6, column=0)

root.mainloop()
