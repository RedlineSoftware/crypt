import random

sor=["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]

def main_coding_decoding(text:str, off:int, decoding:bool):
    ki=[]
    if off == 0: 
        off=random.randint(1, 25)
    for betu in text:
        if decoding:
                for i in range(len(sor)):
                    if betu == sor[i]:
                        if i - off < 0:
                            ki.append(sor[(i - off) % 26])
                        else:
                            ki.append(sor[i - off])
                if betu not in sor:
                    ki.append(betu)
        else:
            for i in range(len(sor)):        
                if betu == sor[i]:
                    if i + off > 25:
                        ki.append(sor[(i + off) % 26])
                    else:
                        ki.append(sor[i + off])
            if betu not in sor:
                ki.append("?")

    return "".join(ki)
