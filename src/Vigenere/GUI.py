import copy
import vigenere as v
from tkinter import *
import pathlib

v.GenerateTable()

# Copied from style.py
# A style.py-ból bemásolva
BG_COLOR = "#2E3440"
FG_COLOR = "#ECEFF4"
ENTRY_BG_COLOR = "#3B4252"
BUTTON_BG_COLOR = "#4C566A"

def clear():
    inputField.delete(0, END)
    keyfield.delete(0, END)
    result.delete("1.0", "end")

def copyans():
    root.clipboard_clear()
    root.clipboard_append(result.get("1.0", END).strip())

def back_to_main():
    root.destroy()




#VALUEERROR?
def EN():
    result.delete("1.0", "end")
    try:
        result.insert("1.0",v.Encrypt(inputField.get(), v.GenerateKey(inputField.get(),keyfield.get())))
    except ValueError:
        result.insert("1.0", "Invalid key!")

#VALUEERROR?  
def DE():
    result.delete("1.0", "end")
    try:
        result.insert("1.0", v.Decrypt(inputField.get(), v.GenerateKey(inputField.get(), keyfield.get())))
    except ValueError:
        result.insert("1.0", "Invalid key!")

root = Tk()
root.title("Vigenere cipher")
root.resizable(False, False)
root.config(bg=BG_COLOR)

program_path = pathlib.Path(__file__).parent.parent.absolute()
root.iconphoto(False, PhotoImage(file=str(program_path) + "/icon.png"))

back_button = Button(root, text="Back", bg=BUTTON_BG_COLOR, fg=FG_COLOR, command=back_to_main)
back_button.grid(row=0, column=0)


#CLEAR
clearb = Button(root, bg = "#1AFF1A", command = clear, text = "CLEAR")

clearb.grid(row = 0, column = 1, columnspan = 2, sticky = W)

#COPY
copyb =  Button(root, bg = "yellow", command = copyans, text = "COPY")
copyb.grid(row = 0, column = 0, sticky =  E)

#INPUT
text = Label(root, text = "Input", bg=BG_COLOR, fg=FG_COLOR)
text_1 = Label(root, text = "Key", bg=BG_COLOR, fg=FG_COLOR)
inputField = Entry(root, width = 45, relief =  SUNKEN)
keyfield =  Entry(root, relief =  SUNKEN)
ENbutton =  Button(root, bg = BUTTON_BG_COLOR, fg=FG_COLOR, command = EN, text = "Encrypt!")
DEbutton =  Button(root, bg = BUTTON_BG_COLOR, fg=FG_COLOR, command = DE, text = "Decrypt!")

text.grid(row = 1, column = 0)
inputField.grid(row = 2, column = 0)
text_1.grid(row = 3, column = 0)
keyfield.grid(row = 4 , column = 0)
ENbutton.grid(row = 5, column = 0, padx = 4, pady = 2, sticky = W)
DEbutton.grid(row = 5, column = 0, padx = 4, pady = 2, sticky = E)

#OUTPUT
result = Text(root, width = 50, height = 5, relief =  SUNKEN)
result.grid(row = 6, column = 0)
root.mainloop()