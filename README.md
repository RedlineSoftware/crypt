# CRVER
---------------------------

CRVER \[no official pronunciation] is a simple cryptographic program that can encode into and decode from 5 cryptosystems.

It can be used for simple secret communication or educational purposes.

-------------------------

A CRVER \[nincs hivatalos kiejtés] egy egyszerű kriptográfiai program, ami 5 féle kódolással képes szövegeket titkosítani és visszafejteni.

Használható egyszerű titkos személyes kommunikációhoz vagy oktatási célra.

-------------------------

## Supported cryptosystems / Támogatott kódolások

- Caesar
- ROT13
- Vigenere
- Enigma
- RSA

--------------------------
## Installation / Telepítés

### Arch Linux

Download and install the latest Pacman package from [GitLab's releases section](https://gitlab.com/RedlineSoftware/crypt/-/releases).

Töltse le és telepítse a legfrissebb Pacman csomagot a [GitLab "releases" szekciójából](https://gitlab.com/RedlineSoftware/crypt/-/releases).

### Other / Más

Use the latest source tarball from the [releases](https://gitlab.com/RedlineSoftware/crypt/-/releases).

Használja a legfrissebb forrásarchívumot a ["releases"-ből](https://gitlab.com/RedlineSoftware/crypt/-/releases).

---------------------------
## Trivia / Érdekesség

The name "CRVER" consists of the initials of the five supported cryptosystems.

A "CRVER" név az öt támogatott kódolás kezdőbetűiből áll.
